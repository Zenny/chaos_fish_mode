#![windows_subsystem = "windows"]

mod poke;

use crate::poke::generate;
use iced::*;
use rand::Rng;
use std::path::PathBuf;

fn main() -> iced::Result {
    Main::run(Settings {
        window: window::Settings {
            size: (960, 290),
            resizable: false,
            ..Default::default()
        },
        ..Default::default()
    })
}

fn string_slice_path(p: &PathBuf) -> String {
    let s = p.to_string_lossy().to_string();
    if s.len() > 80 {
        format!("{}...", s[0..81].to_string())
    } else {
        s
    }
}

enum Main {
    Normal {
        save_loc: Option<PathBuf>,
        rom: Option<PathBuf>,
        ability: bool,
        stat: bool,
        safe_moves: bool,
        unique_moves: bool,
        single: bool,
        fast_lvl: bool,
        seed: Option<u64>,
        error: String,

        go_button: button::State,
        go_button2: button::State,
        rom_button: button::State,
        out_button: button::State,
        seed_box: text_input::State,
    },
}

#[derive(Debug, Clone)]
pub enum Message {
    GoPressed(bool),
    PickRom,
    PickOut,
    TogAbility,
    TogStat,
    TogSingle,
    TogUnique,
    TogSafe,
    TogFast,
    NewSeed(String),
}

impl Application for Main {
    type Executor = iced::executor::Default;
    type Message = Message;
    type Flags = ();

    fn new(_flags: Self::Flags) -> (Self, Command<Self::Message>) {
        (
            Self::Normal {
                save_loc: None,
                rom: None,
                ability: true,
                stat: true,
                safe_moves: true,
                unique_moves: true,
                fast_lvl: true,
                single: false,
                seed: None,
                error: "".to_string(),
                go_button: Default::default(),
                go_button2: Default::default(),
                rom_button: Default::default(),
                out_button: Default::default(),
                seed_box: Default::default(),
            },
            Command::none(),
        )
    }

    fn title(&self) -> String {
        "Chaos Fish Mode v0.3.6".to_string()
    }

    fn update(
        &mut self,
        message: Self::Message,
        _clipboard: &mut Clipboard,
    ) -> Command<Self::Message> {
        match message {
            Message::GoPressed(new_seed) => {
                let Self::Normal {
                    rom,
                    save_loc,
                    ability,
                    stat,
                    single,
                    safe_moves,
                    fast_lvl,
                    unique_moves,
                    seed,
                    error,
                    ..
                } = self;
                if rom.is_none() || save_loc.is_none() {
                    return Command::none();
                }

                if seed.is_none() || new_seed {
                    let mut rng = rand::thread_rng();
                    *seed = Some(rng.gen());
                }

                if let Err(e) = generate(
                    rom.as_ref().unwrap(),
                    seed.as_ref().unwrap(),
                    *ability,
                    *stat,
                    *single,
                    *safe_moves,
                    *fast_lvl,
                    *unique_moves,
                    save_loc.as_ref().unwrap(),
                ) {
                    *error = e.to_string();
                    return Command::none();
                };
            }
            Message::PickRom => {
                let Self::Normal { rom, .. } = self;
                if let Some(v) = rfd::FileDialog::new()
                    .set_title("Pick ROM")
                    .add_filter("GBA ROM", &["gba"])
                    .pick_file()
                {
                    *rom = Some(v);
                }
            }
            Message::PickOut => {
                let Self::Normal { save_loc, .. } = self;
                if let Some(v) = rfd::FileDialog::new()
                    .set_title("Pick Output Dir")
                    .pick_folder()
                {
                    *save_loc = Some(v);
                }
            }
            Message::TogAbility => {
                let Self::Normal { ability, .. } = self;
                *ability = !*ability;
            }
            Message::TogStat => {
                let Self::Normal { stat, .. } = self;
                *stat = !*stat;
            }
            Message::TogSingle => {
                let Self::Normal { single, .. } = self;
                *single = !*single;
            }
            Message::TogUnique => {
                let Self::Normal { unique_moves, .. } = self;
                *unique_moves = !*unique_moves;
            }
            Message::TogSafe => {
                let Self::Normal { safe_moves, .. } = self;
                *safe_moves = !*safe_moves;
            }
            Message::TogFast => {
                let Self::Normal { fast_lvl, .. } = self;
                *fast_lvl = !*fast_lvl;
            }
            Message::NewSeed(new) => {
                let Self::Normal { seed, .. } = self;
                if new.parse::<u64>().is_ok() {
                    *seed = new.parse().ok();
                } else if new.is_empty() {
                    *seed = None;
                }
            }
        }
        Command::none()
    }

    fn view(&mut self) -> Element<'_, Self::Message> {
        let content = match self {
            Main::Normal {
                save_loc,
                rom,
                ability,
                stat,
                single,
                unique_moves,
                safe_moves,
                fast_lvl,
                seed,
                error,
                go_button,
                go_button2,
                rom_button,
                out_button,
                seed_box,
            } => {
                let rom_row = Row::new()
                    .width(Length::Fill)
                    .height(Length::Units(28))
                    .spacing(4)
                    .push(
                        Button::new(rom_button, Text::new("Input ROM")).on_press(Message::PickRom),
                    )
                    .push(
                        Text::new(
                            rom.as_ref()
                                .map_or("No ROM Selected".to_string(), string_slice_path),
                        )
                        .height(Length::Fill)
                        .vertical_alignment(VerticalAlignment::Center)
                        .size(15),
                    );

                let out_row = Row::new()
                    .width(Length::Fill)
                    .height(Length::Units(28))
                    .spacing(4)
                    .push(
                        Button::new(out_button, Text::new("Output Dir")).on_press(Message::PickOut),
                    )
                    .push(
                        Text::new(
                            save_loc
                                .as_ref()
                                .map_or("No Dir Selected".to_string(), string_slice_path),
                        )
                        .height(Length::Fill)
                        .vertical_alignment(VerticalAlignment::Center)
                        .size(15),
                    );

                let abil_row = Row::new().width(Length::Fill).push(Checkbox::new(
                    *ability,
                    "Randomize Ability",
                    |_| Message::TogAbility,
                ));

                let stat_row = Row::new().width(Length::Fill).push(Checkbox::new(
                    *stat,
                    "Randomize Stats",
                    |_| Message::TogStat,
                ));

                let safe_row = Row::new().width(Length::Fill).push(Checkbox::new(
                    *safe_moves,
                    "Safe Movesets",
                    |_| Message::TogSafe,
                ));

                let single_row = Row::new().width(Length::Fill).push(Checkbox::new(
                    *single,
                    "Convert One Starter Only",
                    |_| Message::TogSingle,
                ));

                let unique_row = Row::new().width(Length::Fill).push(Checkbox::new(
                    *unique_moves,
                    "Unique Moves",
                    |_| Message::TogUnique,
                ));

                let fast_row = Row::new().width(Length::Fill).push(Checkbox::new(
                    *fast_lvl,
                    "Speedy Leveling",
                    |_| Message::TogFast,
                ));

                let col_left = Column::new()
                    .width(Length::FillPortion(75))
                    .spacing(10)
                    .push(rom_row)
                    .push(out_row)
                    .push(abil_row)
                    .push(stat_row)
                    .push(safe_row)
                    .push(unique_row)
                    .push(fast_row)
                    .push(single_row);

                let gen_btn = if save_loc.is_none() || rom.is_none() {
                    Button::new(go_button, Text::new("Generate"))
                } else {
                    Button::new(go_button, Text::new("Generate")).on_press(Message::GoPressed(false))
                };

                let gen_btn_seed = if save_loc.is_none() || rom.is_none() {
                    Button::new(go_button2, Text::new("Generate w/ New Seed"))
                } else {
                    Button::new(go_button2, Text::new("Generate w/ New Seed")).on_press(Message::GoPressed(true))
                };

                #[allow(unused_parens)]
                let col_right = Column::new()
                    .width(Length::FillPortion(25))
                    .spacing(10)
                    .push(
                        TextInput::new(
                            seed_box,
                            "Random Seed",
                            (seed
                                .as_ref()
                                .map_or(String::new(), |s| s.to_string())
                                .as_str()),
                            |v| Message::NewSeed(v),
                        )
                        .padding(4),
                    )
                    .push(gen_btn)
                    .push(gen_btn_seed)
                    .push(
                        Text::new(error.clone())
                            .color(Color::new(1.0, 0.0, 0.0, 1.0))
                            .size(15),
                    );

                let row = Row::new()
                    .width(Length::Fill)
                    .padding(20)
                    .push(col_left)
                    .push(col_right);

                row
            }
        };

        Container::new(content)
            .width(Length::Fill)
            .height(Length::Fill)
            .center_x()
            .center_y()
            .into()
    }
}
