use anyhow::Context;
use rand::prelude::SliceRandom;
use rand::{Rng, SeedableRng};
use rand_chacha::ChaCha8Rng;
use std::collections::{HashMap, HashSet};
use std::fs;
use std::path::{Path, PathBuf};
use pokemodder::{Ability, Evolution, Modder, Mon, MonString, FromPrimitive, LearnSet, Move, NUM_TRAINERS, Growth, NUM_HIDDEN_MACHINES, SYM_ADDR_TO_FILE, Addressed};

fn rand_magikarp_stats(rng: &mut ChaCha8Rng) -> (u8, u8, u8, u8, u8, u8) {
    // note the order
    let mut hp = 1;
    let mut atk = 1;
    let mut def = 1;
    let mut spd = 1;
    let mut spca = 1;
    let mut spcd = 1;

    let mut pool = 194;

    while pool > 0 {
        let r: u8 = rng.gen_range(0..6);
        let amt = rng.gen_range(1..22).min(pool);
        match r {
            0 => hp += amt,
            1 => atk += amt,
            2 => def += amt,
            3 => spd += amt,
            4 => spca += amt,
            5 => spcd += amt,
            _ => unreachable!(),
        }
        pool -= amt;
    }

    (hp, atk, def, spd, spca, spcd)
}

pub fn generate<P: AsRef<Path>>(
    path: P,
    seed: &u64,
    ability: bool,
    stat: bool,
    single: bool,
    safe: bool,
    fast_lvl: bool,
    unique: bool,
    out: &PathBuf,
) -> Result<(), anyhow::Error> {
    let mut rng = ChaCha8Rng::seed_from_u64(*seed);
    let mut modder = Modder::open(path, None).context("Failed to open ROM")?;
    // front back color shiny name stats evos learnset battles
    let mk_id = Mon::Magikarp as u16;
    let (_, mk_front) = modder.get_front_pic_ptr(mk_id).context("Couldn't get magikarp front ptr")?.unwrap();
    let mk_front = mk_front.to_owned();
    let (_, mk_front_coords) = modder.get_front_pic_coords(mk_id).context("Couldn't get magikarp front coords")?.unwrap();
    let mk_front_coords = mk_front_coords.to_owned();
    let (_, mk_back) = modder.get_back_pic_ptr(mk_id).context("Couldn't get magikarp back ptr")?.unwrap();
    let mk_back = mk_back.to_owned();
    let (_, mk_back_coords) = modder.get_back_pic_coords(mk_id).context("Couldn't get magikarp back coords")?.unwrap();
    let mk_back_coords = mk_back_coords.to_owned();
    let (_, mk_color) = modder.get_palette_ptr(mk_id).context("Couldn't get magikarp palette")?.unwrap();
    let mk_color = mk_color.to_owned();
    let (_, mk_shiny) = modder.get_shiny_palette_ptr(mk_id).context("Couldn't get magikarp shiny")?.unwrap();
    let mk_shiny = mk_shiny.to_owned();
    let (_, mk_icon) = modder.get_icon_ptr(mk_id).context("Couldn't get magikarp icon")?.unwrap();
    let mk_icon = mk_icon.to_owned();
    let (_, mut mk_stats) = modder.get_base_stats(mk_id).context("Couldn't get magikarp stats")?.unwrap();

    let mut learnset_cache = HashMap::new();
    let mut stat_cache = HashMap::new();

    // porygon2 huntail and gorebyss are being replaced
    let mut starters: Vec<(u16, u16)> = vec![(1, 233), (4, 374), (7, 375)];
    starters.shuffle(&mut rng);

    // Allow hidden moves to be deleted
    for h in 0..NUM_HIDDEN_MACHINES {
        modder.set_hm_move(h, Move::None).expect("Couldn't set hm moves to null");
    }

    let noops = [0, 0, 0, 0];

    if let Some(sym) = modder.get_sym_data("ScrCmd_checkpartymove") {
        let addr = sym.addr + 48 - SYM_ADDR_TO_FILE;
        modder.overwrite(Addressed::new(addr, &noops));
    }

    if let Some(sym) = modder.get_sym_data("PartyHasMonWithSurf") {
        // movs r0, #1
        // bx lr
        let mut ret_true_fn_thumb = vec![0; sym.len];
        ret_true_fn_thumb[0] = 0x01;
        ret_true_fn_thumb[1] = 0x20;
        ret_true_fn_thumb[2] = 0x70;
        ret_true_fn_thumb[3] = 0x47;

        let addr = sym.addr - SYM_ADDR_TO_FILE;
        modder.overwrite(Addressed::new(addr, &ret_true_fn_thumb));
    }

    modder.set_max_flash_level(0).expect("Couldn't set flash level");

    // Overwrite the starters
    for (orig_mon, mon) in starters {
        // Return to original ordering due to a weird bug with shuffling and battles
        let ind = if orig_mon == 1 { 0 } else if orig_mon == 7 { 1 } else { 2 };

        modder.set_starter_mon(ind, Mon::from_u16(mon).expect("I put a bad mon")).expect("Couldn't set starter");

        modder.set_front_pic_ptr(mon, &mk_front).context("Failed set starter front")?;
        modder.set_front_pic_coords(mon, &mk_front_coords).context("Failed set starter front coords")?;
        modder.set_back_pic_ptr(mon, &mk_back).context("Failed set starter back")?;
        modder.set_back_pic_coords(mon, &mk_back_coords).context("Failed set starter back coords")?;
        modder.set_palette_ptr(mon, &mk_color).context("Failed set starter palette")?;
        modder.set_shiny_palette_ptr(mon, &mk_shiny).context("Failed set starter shiny palette")?;
        modder.set_icon_ptr(mon, &mk_icon).context("Failed set starter icon")?;

        modder.set_mon_name(mon, MonString::from_eng("MAGIKARP").unwrap()).context("Failed set name")?;

        // Clear evos
        modder.set_evolutions(mon, [Evolution::default(); 5]).context("Failed set evos")?;

        if stat {
            let (hp, atk, def, spd, spca, spcd) = rand_magikarp_stats(&mut rng);
            mk_stats.base_hp = hp;
            mk_stats.base_atk = atk;
            mk_stats.base_def = def;
            mk_stats.base_spd = spd;
            mk_stats.base_spdef = spcd;
            mk_stats.base_spatk = spca;
        }

        if fast_lvl {
            mk_stats.growth_rate = Growth::Fast;
        }
        if ability {
            mk_stats.ability1 = Ability::from_u8(rng.gen_range(Ability::Stench as u8..=Ability::AirLock as u8)).unwrap();
            // Patch for modified roms that don't respect the default
            if mk_stats.ability2 != Ability::None {
                mk_stats.ability2 = Ability::None;
            }
        }

        stat_cache.insert(orig_mon, mk_stats.clone());
        modder.set_base_stats(mon, mk_stats.clone()).context("Failed set base stats")?;

        let mut learnsets;
        let mut moves;
        let mut in_a_row;
        'trap: loop {
            learnsets = Vec::with_capacity(100);
            moves = HashSet::from([0]);
            in_a_row = 0;
            for lvl in 1..=100 {
                let mut ls = LearnSet {
                    level: lvl,
                    mv: Move::None,
                };
                while moves.contains(&(ls.mv as u16)) {
                    ls.mv = Move::from_u16(rng.gen_range(Move::Pound as u16..=Move::PsychoBoost as u16)).unwrap();
                }
                if safe && ls.mv.does_no_damage() {
                    in_a_row += 1;
                    if in_a_row == 4 {
                        in_a_row = 0;
                        ls.mv = Move::None;
                        let mut loops = 0;
                        while moves.contains(&(ls.mv as u16)) {
                            while ls.mv.does_no_damage() {
                                ls.mv = Move::from_u16(rng.gen_range(Move::Pound as u16..=Move::PsychoBoost as u16)).unwrap();
                            }
                            loops += 1;
                            if loops > 800 {
                                // This moveset didn't work out
                                continue 'trap;
                            }
                        }
                    }
                } else {
                    in_a_row = 0;
                }
                if unique {
                    moves.insert(ls.mv as u16);
                }
                learnsets.push(ls);
            }
            break 'trap;
        }
        modder.set_learn_set(mon, learnsets.clone()).context("Failed set learnsets")?;
        learnset_cache.insert(orig_mon, learnsets.clone());

        for t in 1..NUM_TRAINERS {
            let mut trainer = modder.get_trainer(t).context(format!("Failed get trainer {}", t))?;
            if trainer.name.to_eng() != "TERRY".to_string() {
                continue;
            }
            let mut new_party = trainer.get_party().clone();
            for pmon in new_party.iter_mut() {
                if let Some(pmon) = pmon {
                    let is_swappable = pmon.species as u16 == orig_mon || pmon.species as u16 == orig_mon + 1 || pmon.species as u16 == orig_mon + 2;
                    // catch evos
                    if !is_swappable {
                        continue;
                    }

                    pmon.species = Mon::from_u16(mon).unwrap();

                    // Clear custom moves
                    if trainer.custom_moves {
                        pmon.moves = Some([
                            learnsets.get((pmon.lvl as usize).saturating_sub(1)).map(|v| v.mv).unwrap_or(Move::None),
                            learnsets.get((pmon.lvl as usize).saturating_sub(2)).map(|v| v.mv).unwrap_or(Move::None),
                            learnsets.get((pmon.lvl as usize).saturating_sub(3)).map(|v| v.mv).unwrap_or(Move::None),
                            learnsets.get((pmon.lvl as usize).saturating_sub(4)).map(|v| v.mv).unwrap_or(Move::None),
                        ]);
                    }
                }
            }
            trainer.set_party(new_party);
            modder.set_trainer(t, &mut trainer).context(format!("Failed set trainer {}", t))?;
        }

        if single {
            break;
        }
    }

    let mut spoiler = String::with_capacity(1000*1000);
    macro_rules! spoiler_side {
        ($lbl:literal, $mon:expr) => {
            spoiler.push_str(&format!("//--------\n// {}\n//--------\n", $lbl));
            if let Some(stats) = stat_cache.get(&$mon) {
                spoiler.push_str(&format!("Ability: {:?}\n", stats.ability1));
                spoiler.push_str(&format!("HP: {}\n", stats.base_hp));
                spoiler.push_str(&format!("Attack: {}\n", stats.base_atk));
                spoiler.push_str(&format!("Defense: {}\n", stats.base_def));
                spoiler.push_str(&format!("Speed: {}\n", stats.base_spd));
                spoiler.push_str(&format!("Sp Defense: {}\n", stats.base_spdef));
                spoiler.push_str(&format!("Sp Attack: {}\n", stats.base_spatk));
            }
            if let Some(learn) = learnset_cache.get(&$mon) {
                for ls in learn {
                    spoiler.push_str(&format!("Lv. {} - {:?}\n", ls.level, ls.mv));
                }
            }
        }
    }

    spoiler_side!("Left", 1);
    spoiler_side!("Middle", 7);
    spoiler_side!("Right", 4);

    fs::write(out.join(format!("{} - {:?} spoiler.txt", seed, modder.version)), spoiler).context("Failed to save spoiler file")?;

    modder.save(out.join(format!("{} - Pokemon {:?} - Chaos Fish Version.gba", seed, modder.version))).context("Failed to save")
}
