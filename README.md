# Chaos Fish Mode
A Fire Red Randomizer of Magikarp Madness.

Starters are now Magikarps with a new random attack each level, random stats, and random abilities.

## Download

A Win10 (Maybe Win11?) built binary is provided:

[Visit Downloads Page](https://gitlab.com/Zenny/chaos_fish_mode/-/releases/)

## Build
To build/run yourself, [install rust](https://rustup.rs/) and run:

```
cargo run --release
```

in the workspace directory.

## How to use
1. Click `Input ROM` and select a valid Fire Red or Leaf Green ROM (v1.0 or v1.1).
2. Click `Output Dir` and select where the generated ROMs will be saved.
3. Select some options.
4. Enter your own randomizer seed if you want (numbers only).
5. Click `Generate`.

## Options
- `Randomize Ability` - Randomize the first ability or default to Magikarp's Swift Swim.
- `Randomize Stats` - Randomize the stats or use Magikarp's default stats.
- `Safe Movesets` - Prevents 4 moves in a row being non-damaging moves.
- `Unique Moves` - Limits a move to show up once in the Magikarp's entire moveset from 1-100. May cause slower generation.
- `Speedy Leveling` - Sets the Magikarp's growth type to Fast.
- `Convert One Starter Only` - Only turn 1 of the 3 starters into a Chaos Magikarp.

## Credit
An idea by [xwater](https://www.twitch.tv/xwater), realized by Zenny.